# UX design en onderzoek

## Omschrijving
Dit project bevat het UX ontwerp en de onderzoeksresultaten van bruikbaarheid en functie van het Huishoudboekje voor deelnemende inwoners.

<!--
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

<!-- TODO
## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
-->

<!--
## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.
-->

## Routekaart
- UX-strategie bepalen
- Bestaande informatie lezen
- Expert review met front-office medewerkers
- 1e ruwe prototype maken van alle niveaus
- Fase en stressniveau kiezen om te testen
- Testen voorbereiden
- Prototypen
- Interviews

### Expert review met front-office medewerkers
Er zijn drie interviews gedaan met front-office (FO) medewerkers van Huishoudboekje bij gemeente Utrecht. De interviews waren op 2-11-2021 en 18-11-2021. Dit is een samenvatting van inzichten die in deze interviews verzameld zijn.

#### Personen
- Buurtteams hebben een belangrijke rol bij het begeleiden bij andere problemen/moeilijkheden van een deelnemer.
- FO'ers kunnen drie categorieen aanwijzen van deelnemers die contact opnemen met ze:
  - Het gaat prima met deelnemer. Er wordt weinig contact opgenomen met de FO-medewerker. Dit is prima, want het gaat goed.
  - Het gaat niet goed met de deelnemer maar de deelnemer neemt geen contact op (wil niet / kan niet). Deze groep is moeilijk te bereiken.
  - De groep die heel veel contact opneemt met de FO-medewerker. Veel financiele hulp nodig. En soms ook vragen die verder gaan dan financiële hulp die het HHboekje biedt.
  - Er is een kleine groep mensen die een grote hoeveelheid aandacht krijgt van de begeleiders
  - Meeste deelnemers hebben een mobiele telefoon
  - Whatsapp en internetbankieren zijn bekend bij het grootste gedeelte van de inwoners
  - Meeste deelnemers die contact openemen met de FO-er gaan  bellen of whatsappen

#### Proces
- FO zien het HHB als dienst (persoonlijke begeleiden) en niet alleen als tool/app
- Als er veel contact opgenomen wordt dan is er _iets_ aan de hand, maar dat is niet per se een financieel probleem.
  - Eenzaamheid werd vaak genoemd.
- FO-er zoekt middelen om sneller signalen op te pikken om een deelnemer te kunnen helpen
  - Voorbeeld signaal: Deelnemer belt regelmatig voor een voorschot / of extra geld
  - Voorbeeldsignaal: Er wordt geen bedrag afgescheven voor een vaste last waar dat eerst wel gebeurde
- Regelingen van de gemeente kunnen de deelnemers echt helpen maar de FO'ers weten vaak niet welke er zijn
  - W&I weet af en toe niet van het HHB en HHB weet af en toe niet van W&I-regelingen. Voorbeeld: witgoedregeling.

#### Stressniveaus
- Het lijkt dat er 3 fases zijn :
  - niet redzaam
  - een beetje redzaam
  - wel redzaam
- Het grootste deel van de deelnemers komen volgens de FO'ers niet verder dan fase 2/3 (niet of een beetje redzaam)

#### Huishoudboekje app (red. pilot met app voor inwoners)
- Volgens de FO'ers is de app niet overzichtelijk en geeft deze niet genoeg inzicht in de financiele situatie
- Het totaalbeeld van het saldo is niet altijd duidelijk omdat er nog nieuwe afschrijvingen aan zitten te komen.
  - Het begrip van wat de getallen betekenen is er niet bij de deelnemers.
- Volgens de FO'er is er behoefte voor de deelnemers om hun huidige saldo te kunnen zien, maar.....
  - ... als het niet goed gaat (negatief saldo) dan kan dat ook weer paniek veroorzaken.
- Er lijken een paar vragen te zijn aan de FO'ers die steeds terugkomen:
  - Hoeveel leefgeld krijg ik uitbetaald?
  - Wanneer krijg ik mijn leefgeld uitbetaald?
  - Krijg ik mijn leefgeld echt wel op tijd uitbetaald?
  - Wat is mijn saldo?
  - Kan ik extra geld opnemen uit het huishoudboekje?
  - Kan ik een voorschot op mijn leefgeld krijgen?

#### Wensen van de FO'ers
- FO'er zou graag makkelijk berichtjes (relevante informatie) willen sturen naar een grote groep (alle) deelnemers
- FO'er zou het tof vinden als een deelnemer kan zien wat het verwachtte Saldo is (dus Saldo - verwachtte vaste lasten die er aan zitten te komen deze maand)

#### Visuals
![Wat willen we niet?](https://gitlab.com/commonground/huishoudboekje/ux-design-en-onderzoek/-/raw/main/images/01.JPG "Wat willen we niet?")
![Redzaamheid, vaste lasten en stress](https://gitlab.com/commonground/huishoudboekje/ux-design-en-onderzoek/-/raw/main/images/02.JPG "Redzaamheid, vaste lasten en stress")
![Redzaamheid over tijd (1/2)](https://gitlab.com/commonground/huishoudboekje/ux-design-en-onderzoek/-/raw/main/images/03.JPG "Redzaamheid over tijd (1/2)")
![Redzaamheid over tijd (2/2)](https://gitlab.com/commonground/huishoudboekje/ux-design-en-onderzoek/-/raw/main/images/04.JPG "Redzaamheid over tijd (2/)")
![Schets functies](https://gitlab.com/commonground/huishoudboekje/ux-design-en-onderzoek/-/raw/main/images/05.JPG "Schets functies")

<!--
## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
-->

## Auteurs en erkenning
Jeroen du Chatinier, UX designer bij Gemeente Utrecht\
Rene Olling, UX designer bij Gemeente Utrecht\
Robert Jan Verkade, UX designer bij Eend\
Pieter in 't Hout, strategisch aanjager digitale innovatie Sociaal Domein bij Gemeente Utrecht\
Bauke Huijbers, product owner applicatie Huishoudboekje

<!--
## License
For open source projects, say how it is licensed.
-->

<!-- TODO
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->
